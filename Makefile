include Makefile.common

OBJS = cc.o main.o

all: lib

test: tests

lib: libcarComm_charm.a

.PHONY: tests test clean

libcarComm_charm.a: cc.o
	$(CHARMC) ${LD_OPTS} -o $@ $^ ${LIB_LIBS}

cc.o: cc.C carComm.decl.h carComm_interface.h helper_functions.h carComm_charm.h
	$(CHARMC) -c ${OPTS} ${LIB_INCS} $<

carComm.decl.h: cc.ci
	$(CHARMC) -E $< 

distclean: clean
	rm -f libcarComm_charm.a

clean:
	rm -f *.decl.h *.def.h conv-host *.o charmrun *~
	cd tests; make clean;

tests:  lib
	cd tests; make;

test:	tests lib
	cd tests; make test;
