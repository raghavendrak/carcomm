class Box {
  public:
    int x_start;
    int x_end;
    int y_start;
    int y_end;
    int z_start;
    int z_end;

    Box(int x_start_, int x_end_, int y_start_, int y_end_, int z_start_, int z_end_) :
      x_start(x_start_), x_end(x_end_), y_start(y_start_), y_end(y_end_), z_start(z_start_), z_end(z_end_)
    { }
    Box() {}

    void printBox()
    {
      CkPrintf("<%d %d> <%d %d> <%d %d>\n", x_start, x_end, y_start, y_end, z_start, z_end);
    }
};
PUPbytes(Box)

class tupleTable {
  public:
    //int index;
    Box boxDim;
    int gridANo;
    int index_gridATable;

    tupleTable(/*int index_,*/ Box boxDim_, int gridANo_, int index_gridATable_) :
      /*index(index_),*/ boxDim(boxDim_), gridANo(gridANo_), index_gridATable(index_gridATable_)
    { }
    tupleTable() {}
};
PUPbytes(tupleTable)

class tupleTableData : public tupleTable {
  public:
    void *data_in;
};

class reqBoxSplit {
  public:
    Box ovlapBoxDim;
    int tag_y;
    reqBoxSplit(Box ovlapBoxDim_, int tag_y_) :
      ovlapBoxDim(ovlapBoxDim_), tag_y(tag_y_)
    { }
    reqBoxSplit() {}
};

class reqStoreEntry {
  public:
    Box wholeReqBox;
    std::vector<reqBoxSplit> reqBoxPieces;
    void *data_out;
    CkCallback cb;
    int tag_x;
};

class reqSendEntry : public reqBoxSplit {
  public:
    int gridANo;
    int tag_x;
    int gridBNo;
    // int index;
    int index_gridATable;
    reqSendEntry(Box ovlapBoxDim_, int tag_y_, int gridANo_, int tag_x_, int gridBNo_/*, int index_*/, int index_gridATable_)
      : reqBoxSplit(ovlapBoxDim_, tag_y_), gridANo(gridANo_), tag_x(tag_x_), gridBNo(gridBNo_)/*, index(index_)*/, index_gridATable(index_gridATable_)
    { }
    reqSendEntry() 
      : reqBoxSplit()
    {}
};
PUPbytes(reqSendEntry);


void Charm_createCarComm (int N_x, int N_y, int N_z, 
    int totalChares_gridA, int totalChares_gridB, CkCallback initACallback, bool useTRAM = false, CkCallback initBCallback = CkCallback()); 

void Charm_registerInMemory(void *data_in, Box boxDim, 
    CkCallback memcallback/*, int index*/, CProxy_gridA gridAId);

void Charm_getData(void *data_out, Box boxDim,
   CkCallback arrBcallback, CProxy_gridA gridAId);

void Charm_putData(void *data_in, Box boxDim,
   CkCallback arrBcallback, CProxy_gridA gridAId);

void Charm_beginIteration(CkCallback itercallback, CProxy_gridA gridAId);
