bool doBoxesOverlap(Box &a, Box &b)
{
  return ((a.x_start <= b.x_end && a.x_end >= b.x_start) &&
      (a.y_start <= b.y_end && a.y_end >= b.y_start) &&
      (a.z_start <= b.z_end && a.z_end >= b.z_start));
}

Box overlapBox(Box &a, Box &b)
{
  Box c;
  c.x_start = std::max(a.x_start, b.x_start);
  c.y_start = std::max(a.y_start, b.y_start);
  c.z_start = std::max(a.z_start, b.z_start);
  c.x_end = std::min(a.x_end, b.x_end);
  c.y_end = std::min(a.y_end, b.y_end);
  c.z_end = std::min(a.z_end, b.z_end);
  assert(c.x_start >= 0 && c.y_start >= 0 && c.z_start >= 0 && c.x_end >= 0 && c.y_end >= 0 && c.z_end >= 0);
  return c;
}

// data is in the form data[z][x][y]
// sBox always fits inside bBox
// sBox represents the entire box dimension, and puts/gets that portion from bBox depending on bBox_to_sBox
// if bBox_to_sBox = 1 copies from bBox to sBox else sBox to bBox
void copyBoxes(Box bBox, void *data_bBox, Box sBox, void *data_sBox, int bBox_to_sBox)
{
  int sizeX = bBox.x_end - bBox.x_start;
  int sizeY = bBox.y_end - bBox.y_start;
  int sizeZ = bBox.z_end - bBox.z_start;
  double *data_src, *data_dest;
  if(bBox_to_sBox == 1) {
    data_src = (double *) data_bBox;
    data_dest = (double *) data_sBox;
  }
  else {
    data_src = (double *) data_sBox;
    data_dest = (double *) data_bBox;
  }
  int cnt = 0;

  // bBox has a bigger dimension than sBox, and does not start from 0 => data_bBox[0][0][0] holds bBox.x_start
  // sBox starts from 0, so data_sBox[0] can be used
  // sBox.z_start represents the co-ordinate in the bBox that needs to be extracted, and that co-ordinate in turn will start from 0 in the actual data_bBox
  // so i = sBox.x_start - bBox.x_start
  //    j = sBox.y_start - bBox.y_start
  //    k = sBox.z_start - bBox.z_start
  //    data_bBox[i * sizeY * sizeZ + j * sizeZ + k]
  for(int i = sBox.z_start; i < sBox.z_end; i++)
  for(int j = sBox.y_start; j < sBox.y_end; j++)
  for(int k = sBox.x_start; k < sBox.x_end; k++) {
    int ii = i - bBox.z_start;
    int jj = j - bBox.y_start;
    int kk = k - bBox.x_start;
    // CkPrintf("i: %d bBox.x_start: %d j: %d bBox.y_start: %d k: %d bBox.z_start: %d\n", i, bBox.x_start, j, bBox.y_start, k, bBox.z_start);
    // CkPrintf("ii: %d jj: %d kk: %d index: %d\n", ii, jj, kk, (ii * sizeY * sizeX + jj * sizeX + k));
    assert(ii >= 0 && jj >= 0 && kk >= 0);
    // FIXME: optimize this if statement: have a separate function?
    if(bBox_to_sBox == 1) {
      data_dest[cnt++] = data_src[ii * sizeY * sizeX + jj *sizeX + kk];
    }
    else {
      data_dest[ii * sizeY * sizeX + jj * sizeX + kk] = data_src[cnt++];
    }
  }
  int data_sz = (sBox.x_end - sBox.x_start) * (sBox.y_end - sBox.y_start) * (sBox.z_end - sBox.z_start);
  assert (cnt == data_sz);
}

bool validateDim(Box c)
{
  return(c.x_start >= 0 && c.y_start >= 0 && c.z_start >= 0 && c.x_end >= 0 && c.y_end >= 0 && c.z_end >= 0);
}
